﻿using UnityEngine;
using System.Collections;

public class TubosScript : MonoBehaviour {

    //declaramos la velocidad inicial de la columna
    public Vector3 velocidad;
    //La distancia que habra entre una columna y otra
    public Vector3 distanciaEntreColumnas;
    //La forma correcta de hacerlo ¿?
    public SpriteRenderer formaColumna;
    public GUIText puntaje;
    private bool sumarPuntaje = true;
    // Use this for initialization
    public bool juegoIniciado;
    //sonido de cada punto
    public AudioClip sonidoPunto;

    void Start () {
	}

    // Update is called once per frame
    void Update(){
        if(juegoIniciado)
            moverTubo();
    }

    private void moverTubo(){
        //Los tubos iran avanzando de a pocos, igual que el Flappy bird
        this.transform.position = this.transform.position + (velocidad * Time.deltaTime);
        /*
        if(formaColumna.isVisible == true){
            //Le aumentamos la distancia entre columnas al llegar a la posicion -13.5
            Vector3 posicionTemporal = this.transform.position + distanciaEntreColumnas;
            //Cambiamos el lugar en Y por uno random
            posicionTemporal.y = Random.Range(-3f, 0.6f);
            //Movemos a los tubos a esa posicion
            this.transform.position = posicionTemporal;
        }
        */
        if (this.transform.position.x <= -13.5f){
            //Le aumentamos la distancia entre columnas al llegar a la posicion -13.5
            Vector3 posicionTemporal = this.transform.position + distanciaEntreColumnas;
            //Cambiamos el lugar en Y por uno random
            posicionTemporal.y = Random.Range(-3f, 0.6f);
            //Movemos a los tubos a esa posicion
            this.transform.position = posicionTemporal;
            sumarPuntaje = true;
        }

        //Sumamos una vez cuando los tubos pasan al pajaro
        if (this.transform.position.x <= -12.8 & sumarPuntaje == true)
        {
            sumarPuntaje = false;
            int puntos = int.Parse(puntaje.text) + 1;
            puntaje.text = puntos.ToString();
            print("hola");
            //llamamos al sonido
            ReproducirSonido(sonidoPunto);
        }
    }

    //Reproduce un efecto de sonido
    private void ReproducirSonido(AudioClip clipOriginal)
    {
        // Como no es un sonido 3D la posicion no importa
        AudioSource.PlayClipAtPoint(clipOriginal, transform.position);
    }
}