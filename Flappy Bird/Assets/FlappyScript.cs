﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class FlappyScript : MonoBehaviour {

	//Declaramos la velocidad inicial del pajaro sea igual a zero, Vector3.zero = 0,0,0
	//1,1,0
	Vector3 velocidad = Vector3.zero;
	//Declaramos un vector que controle la gravedad, no usaremos la fisica de unity
	public Vector3 gravedad;
	//Declaramos un vector que define el salto (aleteo) del pajaro
	public Vector3 velocidadAleteo;
	//Declaramos si se debe aletear, si se toco la pantalla o se presiono espacio
	bool aleteo = false;
	//Declaramos la velocidad maxima de rotacion del pajaro
	public float velocidadMaxima;

    //Agregamos una referencia de los tubos en flappy para poder modificarlos
    public TubosScript tubo1;
    public TubosScript tubo2;

    private Animator anim;
    private bool juegoTerminado;
    private bool juegoIniciado;

    public RectTransform botonesMenu;

    
    // Use this for initialization
    void Start () {
        anim = this.gameObject.GetComponent<Animator>();
    }

	// Update is called once per frame
	void Update (){

        //aumenta con el numero de presiones en la pantalla
        int numPresiones = 0;
        foreach (Touch toque in Input.touches)
        {
            if (toque.phase != TouchPhase.Ended && toque.phase != TouchPhase.Canceled)
                numPresiones++;
        }

        //Si la persona presiona el boton de espacio o hace clic en la pantalla con el mouse, o tocas con el dedo
        if (Input.GetKeyDown(KeyCode.Space) | Input.GetMouseButtonDown(0) | numPresiones > 0){
            if (juegoTerminado == false)
                aleteo = true;
            juegoIniciado = true;
            tubo1.juegoIniciado = true;
            tubo2.juegoIniciado = true;
        }
        if (juegoTerminado)
            MostrarBotones();

    }

    //Este es el update de la fisica, que es ligeramente mas lento que el update del juego
    void FixedUpdate() {

        if (juegoIniciado)
        {
            //A la velocidad le sumamos la gravedad (Para que el pajaro caiga)
            velocidad += gravedad * Time.deltaTime;

            //Si presionaron espacio o hicieron clic
            if (aleteo == true)
            {
                //Que solo sea una vez
                aleteo = false;
                //El vector velocidad recibe el impulso hacia arriba al pajaro
                velocidad.y = velocidadAleteo.y;
            }
            //Hacemos que el pajaro reciba la velocidad (la gravedad lo hace caer mas rapido)
            transform.position += velocidad * Time.deltaTime;
            float angulo = 0;
            if (velocidad.y >= 0)
            {
                //Cambiamos el angulo si Y es positivo que mire arriba
                angulo = Mathf.Lerp(0, 25, velocidad.y / velocidadMaxima);
            }
            else
            {
                //Cambiamos el angulo si Y es negativo que mire abajo
                angulo = Mathf.Lerp(0, -75, -velocidad.y / velocidadMaxima);
            }
            //Rotamos
            transform.rotation = Quaternion.Euler(0, 0, angulo);
        }

    }

    private void MostrarBotones() {
        botonesMenu.gameObject.SetActive(true);

    }

        //Cada vez que haya una colision con cualquier objeto que tenga un collider se actiavara esta funcion
        //Collider son Box Collider 2D, Circle Collider 2D, etc.
        void OnCollisionEnter2D (Collision2D colision)
    //void OnTriggerEnter2D(Collider2D colision)
    {
            //Si colisionamos con el tubo que se detengan los tubos
            if (colision.gameObject.name == "Tubo1" | colision.gameObject.name == "Tubo2" | colision.gameObject.name == "Piso1")
            {
                //Hacemos que la velocidad de los tubos se haga cero
                tubo1.velocidad = new Vector3(0, 0, 0);
                tubo2.velocidad = new Vector3(0, 0, 0);
                //Dejamos de ejecutar el aleteo(impulso) al hacer clic
                juegoTerminado = true;
                anim.SetTrigger("JuegoTerminado");
  
            //Al momento de caer, queremos ignorar la colision con el tubo de abajo
        if(colision.gameObject.name == "Tubo1")
        {
            colision.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
        //Si colisionamos con el Piso, que la gravedad no siga aumentando
        if(colision.gameObject.name == "Piso1")
        {
            gravedad = new Vector3(0,0,0);
        }


            }

        }




   
}
